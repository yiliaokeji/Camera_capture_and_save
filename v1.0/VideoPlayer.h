#pragma once
#include <QThread>
#include <QImage>
#include <QtConcurrent/qtconcurrentrun.h>

class VideoPlayer :public QThread
{
public:
	VideoPlayer();
	~VideoPlayer();

	void startPlay();

signals:
	void sig_GetOneFrame(QImage); //每获取到一帧图像 就发送此信号
signals:
	void sig_GetRFrame(QImage);

protected:
	void run();

private:
	QString mFileName;
};

